require('newrelic');
var ack = require('ac-koa').require('hipchat');
var pkg = require('./package.json');
var Notifier = require('ac-koa-hipchat-notifier').Notifier;
var app = ack(pkg);

var addon = app.addon()
  .hipchat()
  .allowRoom(true)
  .scopes(['send_notification','send_message']);

if (process.env.DEV_KEY) {
  addon.key(process.env.DEV_KEY);
}

var notifier = Notifier({format: 'html', dir: __dirname + '/messages'});

// /pingbot -> show help message
addon.webhook('room_message', /^\/(pingbot|ping)(\s)*(help)?$/, function *() {
    yield notifier.sendTemplate('help');
});

// ping -> PONG
addon.webhook('room_message', /(^ping)$/i, function () {
    this.roomClient.sendNotification('PONG', {notify: true, format: 'text'});
});

// ping N -> pong N times
addon.webhook('room_message', /^ping\s(\d+)$/, function *() {
  var number = parseInt(this.match[1]);
  for (var i = 1; i <= number; i++) {
      yield this.roomClient.sendNotification('pong ' + i, {notify: true, format: 'text'});
  }
});

/*
/ping me in 5 seconds OR /ping Loren 5 times OR /ping me with {text}
match[1] = /ping
match[2] = (\D*)? -- mentionName
match[3] = with
match [4] = (\C*) -- text to send if "with" is present
match[5] = (\d*)  -- number of times or seconds
match[6] = (second(s?)|time(s?))?
 */
addon.webhook('room_message', /(^\/ping)\s(\w*)?\s?(with\s(.*))?\D*(\d*)\s*(second(s?)|time(s?))?$/, function () {
  var roomClient = this.roomClient;
  var whatToMention = this.match[2] == null || this.match[2] == "me" ? this.sender.mention_name : this.match[2].replace("@","");
  var withExists = this.match[3] != null && this.match[3].contains("with");
  var textToSend = this.match[4];
  var numberOfSecondsOrTimes = parseInt(this.match[5]);
  var secondsOrTimesString = this.match[6] == null ? "seconds" : this.match[6];
  var doCall = function (iteration, last_one) {
    var message = '@' + whatToMention + ' ';
    if (iteration) {
      message += iteration + ' time' + ((iteration > 1)?'s':'');
      if (last_one) {
        message = message + ' (ducreux)'
      }
    } else if (numberOfSecondsOrTimes) {
        message += whatToMention + ' it\'s been ' + numberOfSecondsOrTimes + ' seconds';
    } else if (withExists) {
        message += textToSend;
    }
    roomClient.sendNotification(message, {notify: true, format: 'text'})
  };

  if(secondsOrTimesString.contains("time")) {
    for (var i = 1; i <= numberOfSecondsOrTimes; i++) {
      setTimeout(doCall, (i-1)*1000, i, i == numberOfSecondsOrTimes);
    }
  } else {
    setTimeout(doCall, numberOfSecondsOrTimes*1000);
  }
});



/*
/ping me every 10 seconds for 60 seconds
match[1] = /ping
match[2] = (\D*)?   -- mentionName
match[4] = (\d*)    -- frequency
match[7] = (\d*)    -- total number of seconds
*/
addon.webhook('room_message', /(^\/ping)\s(\w*)?\s*(every)\s*(\d*)\s*(second(s?))\s*for\s*(\d*)\s*(second(s?))$/, function () {
    var roomClient = this.roomClient;
    var whatToMention = this.match[2] == null || this.match[2] == "me" ? this.sender.mention_name : this.match[2].replace("@","");
    var frequency = parseInt(this.match[4]);
    var totalTime = parseInt(this.match[7]);
    var frequencyFunction = function (index, total) {
      roomClient.sendNotification('@' + whatToMention + " " + index + " of " + total, {notify: true, format: 'text'})
    };
    var invocationCount = Math.floor(totalTime/frequency);

    for (var i = 1; i <= invocationCount; i++) {
      setTimeout(frequencyFunction, frequency*i*1000, i, invocationCount);
    }
});



// /pingbot slash
addon.webhook('room_message', /(^\/pingbot)\s(\D*)?(slash)/, function () {
    var _this = this;

    setTimeout(function() {
        _this.roomClient.sendNotification('testing message formatting slash commands:', {notify: true, format: 'text'});
    }, 0);
    setTimeout(function() {
        _this.roomClient.sendNotification('/code Testing /code and keyword highlighting: Public static void new (args[]) exit Chef ' +
            'require \'some green text here\' End Ran  [2015-03-05T12:33:31+00:00] ' +
            'for do in end var int float = 10; ' +
            'printf( "Value of float = %d\n", float);', {notify: true, format: 'text'});
    }, 1000);
    setTimeout(function() {
        _this.roomClient.sendNotification('/quote Testing /quote \n' +
            'Lilu Dallas multipass. (awesome) ', {notify: true, format: 'text'});
    }, 2000);
    setTimeout(function() {
        _this.roomClient.sendNotification('/me is testing /me (mariokart)', {notify: true, format: 'text'});
    }, 3000);
    setTimeout(function() {
        _this.roomClient.sendNotification('/em is testing /em (celeryman)', {notify: true, format: 'text'});
    }, 4000);
    setTimeout(function() {
        _this.roomClient.sendNotification('#205081 #3572b0', {notify: true, format: 'text'});
    }, 5000);
//

});


addon.webhook('room_message', /(^\/pingbot)\s(\D*)?(emoticons|smilies|emojis)/, function () {
    this.roomClient.sendNotification('Emoticons: (pbj) (content) (yey) (poo) (lol) (shipit) (christurkey) (ceilingcat) (potatodance) (mindblown) (selfhighfive) (evilburns) (grindsmygears) (troll) (invader)   (allthethings)', {notify: true, format: 'text'});
    this.roomClient.sendNotification('Smilies: :D :o :Z :p ;p >:-( :( 8) :\'( :# (embarrassed) O:) :-* (oops) :\\ :) :-| ;) (thumbsdown) (thumbsup) ', {notify: true, format: 'text'});
    this.roomClient.sendNotification('Emojis: (ಠᴥಠ)  👽 🐐 👀 👌 👙 🐮 🐼 🐳 🐙 🐩 🐢 🍄 🎉 🎶 📎 🍥 🍌 ⛵️ ➰ 〰 ♥️ ', {notify: true, format: 'text'});
});


addon.webhook('room_message', /(^\/pingbot)\s(\d)*\s*(image(s?))/, function () {
    this.roomClient.shareLinkWithRoom("http://www.google.com");

});


app.listen();
